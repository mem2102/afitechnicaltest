# AFITechnicalTest

Technical Test.
Requires a new SQL Login:
  Login Name: "AnimalFriend"
  SQL Server authentication
  Password: "An1malFr13nd"
  Remove "Enforce password policy".
Server Roles: "sysadmin"

Postman scripts for testing are located in:
\AnimalFriends\AnimalFriends.Tests\Postman

Run through IIS Express, then hit the endpoint with the Postman requests.