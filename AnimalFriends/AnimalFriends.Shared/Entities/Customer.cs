﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AnimalFriends.Shared.Entities
{
    /// <summary>
    /// Entity for registering a customer
    /// </summary>
    public class Customer
    {
        [Key]
        public int CustomerId { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Forename { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Surname { get; set; }

        [Required]
        [RegularExpression(@"^([A-Z]){2}-(\d){6}$")]
        public string PolicyReferenceNumber { get; set; }

        [RegularExpression(@"^([a-zA-Z0-9]){4,}@([a-zA-Z0-9]){2,}(\.com$|\.co\.uk)$")]
        public string EmailAddress { get; set; }

        //TODO Add less than date validation
        public DateTime? DateOfBirth { get; set; }
    }
}
