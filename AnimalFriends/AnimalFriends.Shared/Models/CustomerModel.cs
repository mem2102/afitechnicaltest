﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AnimalFriends.Shared.Models
{
    /// <summary>
    /// Model for registering a customer
    /// </summary>
    public class CustomerModel
    {
        [Required(ErrorMessage = "Please provide a forename.")]
        [MinLength(3, ErrorMessage = "Provided forename must be greater than 3 characters.")]
        [MaxLength(50, ErrorMessage = "Provided forename must be less than 50 characters.")]
        public string Forename { get; set; }

        [Required(ErrorMessage = "Please provide a surname.")]
        [MinLength(3, ErrorMessage = "Provided surname must be greater than 3 characters.")]
        [MaxLength(50, ErrorMessage = "Provided surname must be less than 50 characters.")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Please provide a Policy Reference Number.")]
        [RegularExpression(@"^([A-Z]){2}-(\d){6}$", ErrorMessage = "Policy Reference Number must match the format XX-999999.")]
        public string PolicyReferenceNumber { get; set; }

        [RegularExpression(@"^([a-zA-Z0-9]){4,}@([a-zA-Z0-9]){2,}(\.com$|\.co\.uk)$",
            ErrorMessage = "Please enter a valid email address.")]
        public string EmailAddress { get; set; }

        public DateTime? DateOfBirth { get; set; }
    }
}
