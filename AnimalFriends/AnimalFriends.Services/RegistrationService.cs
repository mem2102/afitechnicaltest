﻿using AnimalFriends.Domain.UnitOfWork;
using AnimalFriends.Services.Interfaces;
using AnimalFriends.Shared.Entities;
using AnimalFriends.Shared.Models;
using AutoMapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using ValidationContext = System.ComponentModel.DataAnnotations.ValidationContext;

namespace AnimalFriends.Services
{
    /// <see cref="IRegistrationService"/>
    public class RegistrationService : IRegistrationService
    {
        private readonly ILogger<RegistrationService> _logger;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public RegistrationService(ILogger<RegistrationService> logger,
            IMapper mapper,
            IUnitOfWork unitOfWork)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        /// <see cref="IRegistrationService.RegisterCustomer(CustomerModel)"/>
        public async Task<int> RegisterCustomer(CustomerModel customerModel)
        {
            try
            {
                ValidateModel(customerModel);
            }
            catch (Exception e)
            {
                throw e;
            }

            var customerDbEntity = _mapper.Map<Customer>(customerModel);
            var registeredCustomerEntity = await _unitOfWork.CustomersRepository.CreateAsync(customerDbEntity);

            _unitOfWork.Save();
            _logger.LogInformation($"Customer {registeredCustomerEntity.CustomerId} registered.");

            return registeredCustomerEntity.CustomerId;
        }

        /// <summary>
        /// Validates the model against DataAnnotations and Acceptance Criteria
        /// </summary>
        /// <param name="customerModel"></param>
        /// <returns></returns>
        private bool ValidateModel(CustomerModel customerModel)
        {
            if (customerModel == null)
            {
                throw new ArgumentNullException(nameof(customerModel));
            }

            List<ValidationResult> validationResults = new List<ValidationResult>();
            var validationContext = new ValidationContext(customerModel, null, null);
            var isValid = Validator.TryValidateObject(customerModel, validationContext, validationResults, true);

            if (!isValid)
            {
                var validationFails = new System.Text.StringBuilder();
                foreach (var fail in validationResults)
                {
                    validationFails.Append(fail.ErrorMessage);
                }

                throw new ArgumentException(nameof(customerModel), validationFails.ToString());
            }

            if (customerModel.EmailAddress == null &&
                (!customerModel.DateOfBirth.HasValue
                || customerModel.DateOfBirth == DateTime.MinValue))
            {
                throw new ArgumentException(nameof(customerModel),
                    "Please provide either policy holder's Date of Birth or email address.");
            }
            if (customerModel.DateOfBirth.HasValue
                && customerModel.DateOfBirth > DateTime.Now.AddYears(-18))
            {
                throw new ArgumentException(nameof(customerModel),
                    "Policy holder must be at least 18 years old.");
            }

            return true;
        }
    }
}
