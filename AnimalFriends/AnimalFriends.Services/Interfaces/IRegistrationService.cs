﻿using AnimalFriends.Shared.Models;
using System.Threading.Tasks;

namespace AnimalFriends.Services.Interfaces
{
    /// <summary>
    /// Service for registering customers
    /// </summary>
    public interface IRegistrationService
    {
        /// <summary>
        /// Register a customer
        /// </summary>
        /// <param name="customerModel"></param>
        /// <returns></returns>
        public Task<int> RegisterCustomer(CustomerModel customerModel);
    }
}
