﻿using AnimalFriends.Shared.Entities;
using AnimalFriends.Shared.Models;
using AutoMapper;

namespace AnimalFriends.Services.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CustomerModel, Customer>();
        }
    }
}
