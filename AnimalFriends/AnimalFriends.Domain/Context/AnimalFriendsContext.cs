﻿using AnimalFriends.Shared.Entities;
using Microsoft.EntityFrameworkCore;

namespace AnimalFriends.Domain.Context
{
    public class AnimalFriendsContext : DbContext
    {
        public AnimalFriendsContext(DbContextOptions<AnimalFriendsContext> options)
            : base(options) { }

        #region DbSets

        public DbSet<Customer> Customers { get; set; }

        #endregion

        #region Configuration

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .HasIndex(c => c.CustomerId)
                .IsUnique();
        }

        #endregion
    }
}
