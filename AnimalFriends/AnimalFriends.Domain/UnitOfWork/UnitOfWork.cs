﻿using AnimalFriends.Domain.Context;
using AnimalFriends.Domain.Repositories;
using AnimalFriends.Domain.Repositories.Interfaces;
using AnimalFriends.Shared.Entities;
using System;

namespace AnimalFriends.Domain.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AnimalFriendsContext _dbContext;
        public IGenericRepository<Customer> CustomersRepository => new GenericRepository<Customer>(_dbContext);

        public UnitOfWork(AnimalFriendsContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void Save()
        {
            _dbContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
