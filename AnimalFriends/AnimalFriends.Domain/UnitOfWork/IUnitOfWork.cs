﻿using AnimalFriends.Domain.Repositories.Interfaces;
using AnimalFriends.Shared.Entities;

namespace AnimalFriends.Domain.UnitOfWork
{
    public interface IUnitOfWork
    {
        IGenericRepository<Customer> CustomersRepository { get; }

        void Save();

        void Dispose();
    }
}
