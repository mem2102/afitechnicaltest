﻿using System.Threading.Tasks;

namespace AnimalFriends.Domain.Repositories.Interfaces
{
    /// <summary>
    /// Generic repository for handling entities
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IGenericRepository<T> where T : class
    {
        /// <summary>
        /// Create an entity in the database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<T> CreateAsync(T entity);
    }
}
