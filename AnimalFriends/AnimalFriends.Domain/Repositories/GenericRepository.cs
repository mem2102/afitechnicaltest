﻿using AnimalFriends.Domain.Context;
using AnimalFriends.Domain.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace AnimalFriends.Domain.Repositories
{
    /// <see cref="IGenericRepository{T}"
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        internal AnimalFriendsContext _context;
        internal DbSet<T> _dbSet;

        public GenericRepository(AnimalFriendsContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }

        /// <see cref="IGenericRepository{T}.CreateAsync(T)"/>
        public async Task<T> CreateAsync(T entity)
        {
            await _dbSet.AddAsync(entity);
            return entity;
        }
    }
}
