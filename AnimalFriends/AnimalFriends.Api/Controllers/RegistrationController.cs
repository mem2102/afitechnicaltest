﻿using AnimalFriends.Services.Interfaces;
using AnimalFriends.Shared.Models;
using AnimalFriends.Shared.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace AnimalFriends.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RegistrationController : ControllerBase
    {
        private readonly ILogger<RegistrationController> _logger;
        private readonly IMapper _mapper;
        private readonly IRegistrationService _registrationService;

        public RegistrationController(
            ILogger<RegistrationController> logger,
            IMapper mapper,
            IRegistrationService registrationService
            )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _registrationService = registrationService ?? throw new ArgumentNullException(nameof(registrationService));
        }

        /// <summary>
        /// Register a customer
        /// </summary>
        /// <param name="customerViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<int>> RegisterCustomer(CustomerViewModel customerViewModel)
        {
            if (customerViewModel == null)
            {
                throw new ArgumentNullException(nameof(customerViewModel));
            }

            try
            {
                var customerModel = _mapper.Map<CustomerModel>(customerViewModel);
                var newCustomerId = await _registrationService.RegisterCustomer(customerModel);
                return newCustomerId;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);

                if (e.InnerException != null)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, e.InnerException.Message);
                }

                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
    }
}
