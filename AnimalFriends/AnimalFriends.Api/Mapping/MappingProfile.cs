﻿using AnimalFriends.Shared.Models;
using AnimalFriends.Shared.ViewModels;
using AutoMapper;

namespace AnimalFriends.Api.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CustomerViewModel, CustomerModel>();
        }
    }
}
