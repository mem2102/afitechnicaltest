using AnimalFriends.Domain.Context;
using AnimalFriends.Domain.UnitOfWork;
using AnimalFriends.Services;
using AnimalFriends.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AnimalFriends.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("AnimalFriendsConnectionString");

            services.AddDbContext<AnimalFriendsContext>(
                options =>
                {
                    options.UseSqlServer(connectionString,
                      sqlOptions =>
                      {
                          sqlOptions.MigrationsAssembly("AnimalFriends.Domain");
                      });
                });

            services.AddAutoMapper(typeof(Services.Mapping.MappingProfile));
            services.AddAutoMapper(typeof(Api.Mapping.MappingProfile));
            services.AddTransient<IRegistrationService, RegistrationService>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, AnimalFriendsContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            context.Database.Migrate();
        }
    }
}
