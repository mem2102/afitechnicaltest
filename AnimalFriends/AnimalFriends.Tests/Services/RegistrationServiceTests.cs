using AnimalFriends.Domain.Repositories.Interfaces;
using AnimalFriends.Domain.UnitOfWork;
using AnimalFriends.Services;
using AnimalFriends.Shared.Entities;
using AnimalFriends.Shared.Models;
using AutoMapper;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace AnimalFriends.Tests.Services
{
    [TestClass]
    public class RegistrationServiceTests
    {
        #region Constructor

        [TestMethod]
        public void GIVEN_RegistrationService_Ctor_WHEN_AllParamsAreNull_THEN_ShouldThrowException()
        {
            //act
            Action act = () =>
            {
                var ctr = new RegistrationService(null,
                null,
                null);
            };

            //assert
            act.Should().Throw<ArgumentNullException>();
        }

        [TestMethod]
        public void GIVEN_RegistrationService_Ctor_WHEN_AllParamsAreValid_THEN_ShouldNotThrowException()
        {
            //arrange
            var loggerMock = new Mock<ILogger<RegistrationService>>();
            var mapperMock = new Mock<IMapper>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();

            //act
            Action act = () =>
            {
                var svc = new RegistrationService(loggerMock.Object,
                    mapperMock.Object,
                    unitOfWorkMock.Object);
            };

            //assert
            act.Should().NotThrow<ArgumentNullException>();
        }

        #endregion

        #region RegisterCustomer

        [TestMethod]
        public async Task GIVEN_RegistrationService_Create_WHEN_CustomerModelIsNull_THEN_ShouldThrowArgumentNullException()
        {
            //arrange
            var loggerMock = new Mock<ILogger<RegistrationService>>();
            var mapperMock = new Mock<IMapper>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();

            var svc = new RegistrationService(loggerMock.Object,
                    mapperMock.Object,
                    unitOfWorkMock.Object);

            // act
            Func<Task> func = async () => { await svc.RegisterCustomer(null); };

            //assert
            await func.Should().ThrowAsync<ArgumentNullException>();
        }

        [TestMethod]
        public async Task GIVEN_RegistrationService_Create_WHEN_InvalidReferenceNo_THEN_ShouldThrowException()
        {
            //arrange
            var loggerMock = new Mock<ILogger<RegistrationService>>();
            var mapperMock = new Mock<IMapper>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            var customersRepositoryMock = new Mock<IGenericRepository<Customer>>();
            var customerModel = new CustomerModel()
            {
                DateOfBirth = DateTime.Now.AddYears(-19),
                Forename = "Test Forename",
                Surname = "Test Surname",
                PolicyReferenceNumber = "fail",
                EmailAddress = "test@test.com"
            };
            var customerEntity = new Customer()
            {
                CustomerId = 1
            };

            customersRepositoryMock.Setup(x => x.CreateAsync(It.IsAny<Customer>()))
                .ReturnsAsync(customerEntity);
            unitOfWorkMock.Setup(x => x.CustomersRepository).Returns(customersRepositoryMock.Object);

            var svc = new RegistrationService(loggerMock.Object,
                    mapperMock.Object,
                    unitOfWorkMock.Object);

            //act
            Func<Task> func = async () => { await svc.RegisterCustomer(customerModel); };

            //assert
            await func.Should().ThrowAsync<ArgumentException>();
        }

        [TestMethod]
        public async Task GIVEN_RegistrationService_Create_WHEN_CreateFails_THEN_ShouldThrowException()
        {
            //arrange
            var loggerMock = new Mock<ILogger<RegistrationService>>();
            var mapperMock = new Mock<IMapper>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            var customersRepositoryMock = new Mock<IGenericRepository<Customer>>();
            var customerModel = new CustomerModel();

            customersRepositoryMock.Setup(x => x.CreateAsync(It.IsAny<Customer>()))
                .Throws(new Exception());
            unitOfWorkMock.Setup(x => x.CustomersRepository).Returns(customersRepositoryMock.Object);

            var svc = new RegistrationService(loggerMock.Object,
                    mapperMock.Object,
                    unitOfWorkMock.Object);

            // act
            Func<Task> func = async () => { await svc.RegisterCustomer(customerModel); };

            //assert
            await func.Should().ThrowAsync<Exception>();
        }

        [TestMethod]
        public async Task GIVEN_RegistrationService_Create_WHEN_AllParamsValid_THEN_ShouldNotThrowException()
        {
            //arrange
            var loggerMock = new Mock<ILogger<RegistrationService>>();
            var mapperMock = new Mock<IMapper>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            var customersRepositoryMock = new Mock<IGenericRepository<Customer>>();
            var customerModel = new CustomerModel() {
                DateOfBirth = DateTime.Now.AddYears(-19),
                Forename = "Test Forename",
                Surname = "Test Surname",
                PolicyReferenceNumber = "XX-999999",
                EmailAddress = "test@test.com"
            };
            var customerEntity = new Customer()
            {
                CustomerId = 1
            };

            customersRepositoryMock.Setup(x => x.CreateAsync(It.IsAny<Customer>()))
                .ReturnsAsync(customerEntity);
            unitOfWorkMock.Setup(x => x.CustomersRepository).Returns(customersRepositoryMock.Object);

            var svc = new RegistrationService(loggerMock.Object,
                    mapperMock.Object,
                    unitOfWorkMock.Object);

            // act
            Func<Task> func = async () => { await svc.RegisterCustomer(customerModel); };

            //assert
            await func.Should().NotThrowAsync<Exception>();
        }

        #endregion Create
    }
}
