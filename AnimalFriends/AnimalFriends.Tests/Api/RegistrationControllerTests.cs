using AnimalFriends.Api.Controllers;
using AnimalFriends.Services.Interfaces;
using AnimalFriends.Shared.Models;
using AnimalFriends.Shared.ViewModels;
using AutoMapper;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace AnimalFriends.Tests.Api
{
    [TestClass]
    public class RegistrationControllerTests
    {
        #region Constructor

        [TestMethod]
        public void GIVEN_RegistrationController_Ctor_WHEN_AllParamsAreNull_THEN_ShouldThrowException()
        {
            //act
            Action act = () =>
            {
                var ctr = new RegistrationController(null,
                null,
                null);
            };

            //assert
            act.Should().Throw<ArgumentNullException>();
        }

        [TestMethod]
        public void GIVEN_RegistrationController_Ctor_WHEN_AllParamsAreValid_THEN_ShouldNotThrowException()
        {
            //arrange
            var loggerMock = new Mock<ILogger<RegistrationController>>();
            var mapperMock = new Mock<IMapper>();
            var registrationServiceMock = new Mock<IRegistrationService>();

            //act
            Action act = () =>
            {
                var ctr = new RegistrationController(loggerMock.Object,
                    mapperMock.Object,
                    registrationServiceMock.Object);
            };

            //assert
            act.Should().NotThrow<ArgumentNullException>();
        }

        #endregion

        #region RegisterCustomer

        [TestMethod]
        public async Task GIVEN_RegistrationController_Create_WHEN_CustomerViewModelIsNull_THEN_ShouldThrowException()
        {
            //arrange
            var loggerMock = new Mock<ILogger<RegistrationController>>();
            var mapperMock = new Mock<IMapper>();
            var registrationServiceMock = new Mock<IRegistrationService>();
            var ctr = new RegistrationController(loggerMock.Object,
                mapperMock.Object,
                registrationServiceMock.Object);

            //act
            Func<Task> func = async () => { await ctr.RegisterCustomer(null); };

            //assert
            await func.Should().ThrowAsync<ArgumentNullException>();
        }

        [TestMethod]
        public async Task GIVEN_RegistrationController_Create_WHEN_CustomerViewModelIsNotNull_THEN_ShouldNotThrowException()
        {
            //arrange
            var loggerMock = new Mock<ILogger<RegistrationController>>();
            var mapperMock = new Mock<IMapper>();
            var registrationServiceMock = new Mock<IRegistrationService>();
            var ctr = new RegistrationController(loggerMock.Object,
                mapperMock.Object,
                registrationServiceMock.Object);
            var customerViewModel = new CustomerViewModel();
            var customerModel = new CustomerModel();
            mapperMock.Setup(x => x.Map<CustomerModel>(It.IsAny<CustomerViewModel>()))
                .Returns(customerModel);

            //act
            Func<Task> func = async () => { await ctr.RegisterCustomer(customerViewModel); };

            //assert
            await func.Should().NotThrowAsync<Exception>();
        }

        [TestMethod]
        public async Task GIVEN_RegistrationController_Create_WHEN_registrationServiceErrors_THEN_ShouldReturnStatusCode500()
        {
            //arrange
            var loggerMock = new Mock<ILogger<RegistrationController>>();
            var mapperMock = new Mock<IMapper>();
            var registrationServiceMock = new Mock<IRegistrationService>();
            var ctr = new RegistrationController(loggerMock.Object,
                mapperMock.Object,
                registrationServiceMock.Object);
            var customerViewModel = new CustomerViewModel();
            registrationServiceMock.Setup(x => x.RegisterCustomer(It.IsAny<CustomerModel>()))
                .Throws(new Exception());

            //act
            var result = await ctr.RegisterCustomer(customerViewModel);

            //assert
            result.Should().NotBeNull();
        }

        #endregion
    }
}
